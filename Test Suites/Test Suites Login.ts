<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suites Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>06b26051-ef00-49c7-b259-aa8fe2f665ed</testSuiteGuid>
   <testCaseLink>
      <guid>da7307a7-2ba0-46ca-be4c-ec963c5cbc4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Page/TC_Login_005</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e8b23567-beb9-45f4-a847-bf628fd53922</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data User</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>e8b23567-beb9-45f4-a847-bf628fd53922</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>ae0a95b5-5fcc-4cb5-9fd6-3b9490900b8d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e8b23567-beb9-45f4-a847-bf628fd53922</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>e46eee2d-1b68-4afb-aa75-bef98cf97061</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
